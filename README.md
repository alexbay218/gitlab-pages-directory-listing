# Gitlab Pages Directory Listing

Gitlab CI CD file for creating nginx-like directory index listing system (see .gitlab-ci.yml).

Will overwrite any index.html files and remove all instances of gitlab_pages_directory_listing.sh.

Example for this repo: https://alexbay218.gitlab.io/gitlab-pages-directory-listing/
